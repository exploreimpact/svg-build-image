<?php

// Default config
const SVG_TEMPLATE = "template.svg.php";
const RENDERED_FILENAME = "status.svg";

const STATUS_FAILING = "failing";
const STATUS_PASSING = "passing";
const STATUS_UNKNOWN = "unknown";

const COLOR_FAILING = "#e05d44";
const COLOR_PASSING = "#4c1";
const COLOR_UNKNOWN = "#9f9f9f";

$message = STATUS_FAILING;
$color = COLOR_FAILING;

$path = './';
$status = STATUS_FAILING;
