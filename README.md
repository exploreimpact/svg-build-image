
SVG Build Images
================
![build passing](status.svg)

### Usage
Expects usage like: `php vendor/bin/buildstatus [status] [directory]`  
Example: `php vendor/bin/buildstatus passing .`

### Resources:
https://shields.io/
